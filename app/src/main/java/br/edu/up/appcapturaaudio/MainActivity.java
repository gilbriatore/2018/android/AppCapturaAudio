package br.edu.up.appcapturaaudio;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

  String path;
  Button btnGravar;
  Button btnParar;
  Button btnTocar;
  MediaRecorder gravador;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnGravar = (Button) findViewById(R.id.btnGravar);
    btnParar = (Button) findViewById(R.id.btnParar);
    btnTocar = (Button) findViewById(R.id.btnTocar);
  }

  public void onClickGravar(View v){

    btnGravar.setEnabled(false);
    btnParar.setEnabled(true);

    try{
      File sdcard = Environment.getExternalStorageDirectory();
      File arquivo = File.createTempFile("audio", ".3gp", sdcard);
      String path = arquivo.getAbsolutePath();

      MediaRecorder gravador = new MediaRecorder();
      gravador.setAudioSource(MediaRecorder.AudioSource.MIC);
      gravador.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
      gravador.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
      gravador.setOutputFile(path);
      gravador.prepare();
      gravador.start();

    } catch (Exception e){
      e.printStackTrace();
    }
  }

  public void onClickParar(View v){

    btnGravar.setEnabled(true);
    btnParar.setEnabled(false);
    btnTocar.setEnabled(true);

    gravador.stop();
    gravador.release();
    gravador = null;
  }

  public void onClickTocar(View v){

    try {

      MediaPlayer mediaPlayer = new MediaPlayer();
      mediaPlayer.setDataSource(path);
      mediaPlayer.prepare();
      mediaPlayer.start();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}